var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({pattern: '*'});

gulp.task('default', ['sass', 'js', 'img', 'browsersync'], function() {
    gulp.watch(['_src/sass/**/*.sass', 'app/libs/**/*'], ['sass']);
    gulp.watch(['_src/libs/**/*.js', '_src/js/**/*.js'], ['js']);
    gulp.watch(['_src/img/**/*'], ['img']);
    
    gulp.watch('public/**/*.html', plugins.browserSync.reload);
});

gulp.task('js', getTask('scripts'));
gulp.task('sass', getTask('sass'));


gulp.task('img', function () {
    gulp.src(['_src/img/**/*'])
        .pipe(gulp.dest('public/img'));
});

gulp.task('browsersync', getTask('browsersync'));


function getTask(task) {
    return require('./gulp-tasks/' + task)(gulp, plugins);
}