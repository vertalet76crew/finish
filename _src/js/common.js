$(document).ready(function () {
  $(".features-img").on("click", function () {
      $(".overlay").show()
    }),
    $(".popup-close").on("click", function () {
      $(".overlay").hide()
    });

  var videos = document.getElementsByClassName("youtube");
  var nb_videos = videos.length;
  for (var i = 0; i < nb_videos; i++) {
    // Находим постер для видео, зная ID нашего видео
    videos[i].style.backgroundImage = 'url(http://i.ytimg.com/vi/' + videos[i].id + '/sddefault.jpg)';

    // Размещаем над постером кнопку Play, чтобы создать эффект плеера
    var play = document.createElement("div");
    play.setAttribute("class", "play");
    videos[i].appendChild(play);

    videos[i].onclick = function () {
      // Создаем iFrame и сразу начинаем проигрывать видео, т.е. атрибут autoplay у видео в значении 1
      var iframe = document.createElement("iframe");
      var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
      if (this.getAttribute("data-params")) iframe_url += '&' + this.getAttribute("data-params");
      iframe.setAttribute("src", iframe_url);
      iframe.setAttribute("frameborder", '0');

      // Высота и ширина iFrame будет как у элемента-родителя
      iframe.style.width = this.style.width;
      iframe.style.height = this.style.height;

      // Заменяем начальное изображение (постер) на iFrame
      this.parentNode.replaceChild(iframe, this);
    }
  }

  $('.feedback-slider').slick({
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<div class="arrow arrow-left"></div>',
    nextArrow: '<div class="arrow arrow-right"></div>'


  });

});





// $(document).ready(function () {
// 	$('.jsShowPopup').on('click', function(){
// 		$('#popup').show();
// 	});
// 	$('.popup-close').on('click', function(){
// 		$('.overlay').hide();
// 	});
// 	$('#header__btn').on('click', function(){
// 		yaCounter50775913.reachGoal('header__btn');
// 	});

// 	$('.jsPhone').mask('+7(999)999-99-99');

// 	$("form").on("submit", function(e){
// 		e.preventDefault();
// 		var form = $(this);

// 		$.ajax({
// 			url: 'mailer/mail.php',
// 			method: 'post',
// 			data: form.serialize(),
// 			success: function(){
// 				$(".overlay").hide();
// 				$("#thx").show();
// 				form.trigger("reset");
// 			}
// 		});
// 	});
// 	new WOW().init();

// 	$(".fabrication__slider_top").slick({
// 	  arrows: false,
// 	  slidesToShow: 1,
// 	  slidesToScroll: 1,
// 	  asNavFor: '.fabrication__slider_bottom',
// 	});
// 	$(".fabrication__slider_bottom").slick({
// 	  arrows: true,
// 	  slidesToShow: 4,
// 	  slidesToScroll: 1,
// 	  prevArrow: '<div class="slider-arrow fabrication__arrow fabrication__arrow_left"></div>',
// 	  nextArrow: '<div class="slider-arrow fabrication__arrow fabrication__arrow_right"></div>',
// 	  asNavFor: '.fabrication__slider_top',
// 	  responsive: [{
// 	      breakpoint: 992,
// 	      settings: {
// 	        slidesToShow: 3,
// 	        slidesToScroll: 1
// 	      }
// 	    },
// 	    {
// 	      breakpoint: 768,
// 	      settings: {
// 	        slidesToShow: 1,
// 	        slidesToScroll: 1
// 	      }
// 	    }
// 	  ]
// 	});


// 	$('.feedback__slider').slick({
// 	  slidesToShow: 3,
// 	  slidesToScroll: 1,
// 	  prevArrow: '<div class="slider-arrow feedback__arrow feedback__arrow_left"></div>',
// 	  nextArrow: '<div class="slider-arrow feedback__arrow feedback__arrow_right"></div>',
// 	  responsive: [{
// 	      breakpoint: 992,
// 	      settings: {
// 	        slidesToShow: 1,
// 	      }
// 	    },
// 	    {breakpoint: 577,
// 	      settings: {
// 	        slidesToShow: 1, 
// 	      }
// 	    }
// 	  ]
// 	});
// 	});
